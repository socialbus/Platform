echo "stopping containers"
docker stop $(docker ps -aq)

echo "removing containers"
docker rm $(docker ps -aq)

echo "removing images"
docker rmi $(docker images -aq)

echo "removing volumes"
docker volume rm $(docker volume ls -f dangling=true -q)
