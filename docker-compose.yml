version: '3.3'

services:
  rabbitmq:
    container_name: "rabbitMQ"
    image: library/rabbitmq:management
    ports:
      - 15672:15672
      - 15671:15671
      - 5672:5672
    volumes:
      - ./webhook-manager/docker/resources/rabbitmq/lib:/var/lib/rabbitmq/mnesia
    networks:
      - socialbus_shared
  mongodb:
    container_name: "mongoDB"
    image: library/mongo
    ports:
      - 27017:27017
    volumes:
      - /var/socialbus/data:/data/db
    networks:
      - socialbus_shared
  messenger:
    container_name: "messenger"
    image: registry.gitlab.inria.fr/socialbus/platform/messenger-webhook:${IMAGE_TAG}
    environment:
      - MESSENGERPORT=${MESSENGERPORT:-5000}
      - MESSENGER_BOT_ID=${MESSENGER_BOT_ID}
      - MESSENGER_VERIFY_TOKEN=${MESSENGER_VERIFY_TOKEN}
      - MESSAGE_AMOUNT_LENGTH=${MESSAGE_AMOUNT_LENGTH}
      - MONGO_HOST=${MONGO_HOST}
      - SLACK_BOT_TOKEN=${SLACK_BOT_TOKEN}
      - TWITTER_CONSUMER_KEY=${TWITTER_CONSUMER_KEY}
      - TWITTER_CONSUMER_SECRET=${TWITTER_CONSUMER_SECRET}
      - TWITTER_TOKEN=${TWITTER_TOKEN}
      - TWITTER_TOKEN_SECRET=${TWITTER_TOKEN_SECRET}
      - MESSENGER_BOT_TOKEN=${MESSENGER_BOT_TOKEN}
      - EXPRESSPORT=${EXPRESSPORT}
      - RABBITMQ_HOST=${RABBITMQ_HOST}
    links:
      - rabbitmq
    depends_on:
      - rabbitmq
      - mongodb
    hostname: 'messenger'
    ports:
      - "${MESSENGERPORT:-5000}:${MESSENGERPORT:-5000}"
    command: ["./wait-for-it.sh", "rabbitmq:5672", "-t", "120", "--", "node", "src/servers/serverFacebook.js"]
    networks:
      - socialbus_shared
  twitter:
    container_name: "twitter"
    image: registry.gitlab.inria.fr/socialbus/platform/twitter-webhook:${IMAGE_TAG}
    environment:
      - TWITTERPORT=${TWITTERPORT:-5001}
      - TWITTER_BOT_ID=${TWITTER_BOT_ID}
      - MESSAGE_AMOUNT_LENGTH=${MESSAGE_AMOUNT_LENGTH}
      - MONGO_HOST=${MONGO_HOST}
      - SLACK_BOT_TOKEN=${SLACK_BOT_TOKEN}
      - TWITTER_CONSUMER_KEY=${TWITTER_CONSUMER_KEY}
      - TWITTER_CONSUMER_SECRET=${TWITTER_CONSUMER_SECRET}
      - TWITTER_TOKEN=${TWITTER_TOKEN}
      - TWITTER_TOKEN_SECRET=${TWITTER_TOKEN_SECRET}
      - MESSENGER_BOT_TOKEN=${MESSENGER_BOT_TOKEN}
      - EXPRESSPORT=${EXPRESSPORT}
      - RABBITMQ_HOST=${RABBITMQ_HOST}
    volumes:
      - .:/usr/src/app
    links:
      - rabbitmq
    depends_on:
      - rabbitmq
      - mongodb
    ports:
      - "${TWITTERPORT:-5001}:${TWITTERPORT:-5001}"
    command: ["./wait-for-it.sh", "rabbitmq:5672", "-t", "120", "--", "node", "src/servers/serverTwitter.js"]
    networks:
      - socialbus_shared
  slack:
    container_name: "slack"
    image: registry.gitlab.inria.fr/socialbus/platform/slack-webhook:${IMAGE_TAG}
    environment:
      - SLACKPORT=${SLACKPORT:-5002}
      - SLACK_VERIFY_TOKEN=${SLACK_VERIFY_TOKEN}
      - MESSAGE_AMOUNT_LENGTH=${MESSAGE_AMOUNT_LENGTH}
      - MONGO_HOST=${MONGO_HOST}
      - SLACK_BOT_TOKEN=${SLACK_BOT_TOKEN}
      - TWITTER_CONSUMER_KEY=${TWITTER_CONSUMER_KEY}
      - TWITTER_CONSUMER_SECRET=${TWITTER_CONSUMER_SECRET}
      - TWITTER_TOKEN=${TWITTER_TOKEN}
      - TWITTER_TOKEN_SECRET=${TWITTER_TOKEN_SECRET}
      - MESSENGER_BOT_TOKEN=${MESSENGER_BOT_TOKEN}
      - EXPRESSPORT=${EXPRESSPORT}
      - RABBITMQ_HOST=${RABBITMQ_HOST}
    volumes:
      - .:/usr/src/app
    links:
      - rabbitmq
    depends_on:
      - rabbitmq
      - mongodb
    ports:
      - "${SLACKPORT:-5002}:${SLACKPORT:-5002}"
    command: ["./wait-for-it.sh", "rabbitmq:5672", "-t", "120", "--", "node", "src/servers/serverSlack.js"]
    networks:
      - socialbus_shared
  receiver:
    container_name: "receiver"
    image: registry.gitlab.inria.fr/socialbus/platform/receiver-webhook:${IMAGE_TAG}
    environment:
      - MESSAGE_AMOUNT_LENGTH=${MESSAGE_AMOUNT_LENGTH}
      - MONGO_HOST=${MONGO_HOST}
      - SLACK_BOT_TOKEN=${SLACK_BOT_TOKEN}
      - TWITTER_CONSUMER_KEY=${TWITTER_CONSUMER_KEY}
      - TWITTER_CONSUMER_SECRET=${TWITTER_CONSUMER_SECRET}
      - TWITTER_TOKEN=${TWITTER_TOKEN}
      - TWITTER_TOKEN_SECRET=${TWITTER_TOKEN_SECRET}
      - MESSENGER_BOT_TOKEN=${MESSENGER_BOT_TOKEN}
      - EXPRESSPORT=${EXPRESSPORT}
      - RABBITMQ_HOST=${RABBITMQ_HOST}
    links:
      - rabbitmq
    depends_on:
      - rabbitmq
      - mongodb
    command: ["./wait-for-it.sh", "rabbitmq:5672", "-t", "120", "--", "bash", "launchReceive.sh"]
    networks:
      - socialbus_shared
  reverse-proxy:
    container_name: "reverse-proxy"
    image: registry.gitlab.inria.fr/socialbus/platform/reverse-proxy-webhook:${IMAGE_TAG}
    environment:
      - MESSENGERPORT=${MESSENGERPORT:-5000}
      - TWITTERPORT=${TWITTERPORT:-5001}
      - SLACKPORT=${SLACKPORT:-5002}
      - NGINXPORT=${NGINXPORT:-5004}
      - EXPRESSPORT=${EXPRESSPORT:-3001}
    ports:
      - ${NGINXPORT:-5004}:${NGINXPORT:-5004}
    env_file:
      - .env
    volumes:
      - ./webhook-manager/docker/nginx/templates:/etc/nginx/templates
      - /usr/share/nginx/html/:/usr/share/nginx/html
      - $WEBSITEVOLUME:$WEBSITEVOLUME
    links:
      - messenger
      - twitter
      - slack
      - express
    depends_on:
      - messenger
      - twitter
      - slack
      - express
    #command: /bin/bash -c "envsubst '\$NGINXPORT \$EXPRESSPORT \MESSENGERPORT \TWITTERPORT \SLACKPORT' < /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'"
    tty: true
    stdin_open: true
    networks:
      - socialbus_shared
  express:
    container_name: "express"
    image: registry.gitlab.inria.fr/socialbus/platform/back-website:${IMAGE_TAG}
    environment:
      - HOSTNAME=${HOSTNAME}
      - SLACK_CLIENT_ID=${SLACK_CLIENT_ID}
      - SLACK_CLIENT_SECRET=${SLACK_CLIENT_SECRET}
      - TWITTER_CONSUMER_KEY=${TWITTER_CONSUMER_KEY}
      - TWITTER_CONSUMER_SECRET=${TWITTER_CONSUMER_SECRET}
      - TWITTER_TOKEN=${TWITTER_TOKEN}
      - TWITTER_TOKEN_SECRET=${TWITTER_TOKEN_SECRET}
      - MESSENGERPORT=${MESSENGERPORT}
      - EXPRESSPORT=${EXPRESSPORT:-3000}
      - MONGO_HOST=${MONGO_HOST}
    ports:
      - ${EXPRESSPORT:-3000}:${EXPRESSPORT:-3000}
    links:
      - mongodb
    depends_on:
      - mongodb
    networks:
      - socialbus_shared
    volumes:
      - "/etc/timezone:/etc/timezone:ro"
      - "/etc/localtime:/etc/localtime:ro"
    command: ["./wait-for-it.sh", "mongodb:27017", "-t", "120", "--", "node", "src/back/routing.js"]
  react:
    container_name: "website"
    image: registry.gitlab.inria.fr/socialbus/platform/react-website:${IMAGE_TAG}
    environment:
      - REACT_APP_EXPRESS_PATH=${REACT_APP_EXPRESS_PATH}
      - REACT_APP_PROTOCOL=${REACT_APP_PROTOCOL}
      - REACT_APP_SLACK_CLIENT_ID=${REACT_APP_SLACK_CLIENT_ID}
      - WEBSITEVOLUME=${WEBSITEVOLUME}
    networks:
      - socialbus_shared
    links:
      - express
    volumes:
      - $WEBSITEVOLUME:/build
    command: bash -c "npm run build"

networks:
  socialbus_shared:
    driver: bridge
