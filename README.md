# Socialbus Platform

Platform handles the automatic deployment of Socialbus. 
More information can be found in the [documentations](https://socialbus.gitlabpages.inria.fr/webhook-manager/tutorial-deployAndIntegrate.html).

The documentation for webhook-manager can be found [here](https://socialbus.gitlabpages.inria.fr/webhook-manager/)

The documentation for the website repository can be found [here](https://socialbus.gitlabpages.inria.fr/website/)
